# osdevlearning

osdevlearning is an attempt to write something running on bare hardware (without operating system) for learning purposes. osdevlearning is a collection of folders each of which contains a separate project consisiting of source code in assembler and a makefile. The source code is intended to be compiled with Flat Assembler (FASM).

For more information see [the project author's blog](https://dvsav.ru) (if you can read in Russian).

## Getting Started

Install [git version control system](https://git-scm.com/) on your computer.
Launch git. In the command prompt go to the folder where you want to copy osdevlearning to.
Type: `git clone https://dvsav@bitbucket.org/dvsav/osdevlearning.git`
Now go to the folder of interest and run make utility to build the project inside the folder. If everything is ok you will get an .img file containing a boot floppy-disk image. Run virtual machine (Bochs for example) and boot from the floppy-disk image.

## Prerequisites

You need the following software to build and run the project:
- [FASM](http://flatassembler.net/) is needed for source code compilation
- make utility (for building projects easily): for Windows users it can be found either as a part of [Windows Software Development Kit](https://msdn.microsoft.com/microsoft-sdks-msdn) or as one of packages in [Cygwin](https://www.cygwin.com/)
- dd utility (for makng floppy-disk image): for Windows users it can be found as a separate utility [dd for Windows](http://www.chrysocome.net/dd) or as a part of [Cygwin](https://www.cygwin.com/)
- virtual machine (for running projects): for example [Bochs](http://bochs.sourceforge.net/) or [VirtualBox](https://www.virtualbox.org/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
