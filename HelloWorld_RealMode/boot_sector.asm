; HelloWorld real mode
use16               ; generate 16-bit code
org 7C00h           ; the code starts at 0x7C00 memory address

start:
    jmp far dword 0x0000:entr ; makes CS=0, IP=0x7c00
entr:
    xor  ax, ax     ; ax = 0
    mov  ds, ax     ; setup data segment ds=ax=0
    cli             ; when we set up stack we need disable interrupts because stack is involved in interrupts handling
    mov  ss, ax     ; setup stack segment ss=ax=0
    mov  sp, 0x7C00 ; stack will grow starting from 0x7C00 memory address
    sti             ; enable interrupts
 
    mov  si, message
    cld             ; clear direction flag (DF is a flag used for string operations)
    mov  ah, 0Eh    ; BIOS function index (write a charachter to the active video page)
puts_loop:
    lodsb           ; load to al the next charachter located at [si] address in memory (si is incremented automatically because the direction flag DF = 0)
    test al, al     ; zero in al means the end of the string
    jz   puts_loop_exit
    int  10h        ; call BIOS standard video service's function
    jmp  puts_loop
puts_loop_exit:
    cli             ; disable interrupts
    hlt             ; halt the processor
    ;jmp  $         ; alternatively to hlt we could use the infinite loop
  
message db 'Hello World!', 0
finish:
    ; The size of a disk sector is 512 bytes. Boot sector signature occupies the two last bytes.
    ; The gap between the end of the source code and the boot sector signature is filled with zeroes.
    times 510-finish+start db 0 
    db 55h, 0AAh    ; boot sector signature