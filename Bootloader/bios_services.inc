; bios_services.inc
use16 ; generate 16-bit code

; <---- BIOS_ClearScreen ----------
BIOS_ClearScreen:
    mov ax, 0x0003  ; ah=0 means clear screen and setup video mode, al=3 means text mode, 80x25 screen, CGA/EGA adapter, etc.
    int 0x10        ; call BIOS standard video service function
    ret
; -------------------------------->

; <---- BIOS_PrintString ----------
; Prints out a string. The string must be zero-terminated.
; Parameters: word [bp + 4] - address of the string
BIOS_PrintString:
        push bp
        mov bp, sp
        
        ; save registers in stack
        push ax
        push bx
        push si
        
        mov si, [bp + 4]   ; si = string address
        cld                ; clear direction flag (DF is a flag used for string operations)
        mov ah, 0x0E       ; (int 0x10) BIOS function index (write a charachter to the active video page)
        mov bh, 0x00       ; (int 0x10) video page number
    .puts_loop:
        lodsb              ; load to al the next charachter located at [si] address in memory (si is incremented automatically because the direction flag DF = 0)
        test al, al        ; zero in al means the end of the string
        jz .puts_loop_exit
        int 0x10           ; call BIOS standard video service
        jmp .puts_loop
        
    .puts_loop_exit:
        ; restore registers from stack
        pop si
        pop bx
        pop ax
        
        mov sp, bp             
        pop bp
        ret 2
; -------------------------------->

; <---- BIOS_ResetDiskSubsystem ---
; Resets disk subsystem.
; Parameters: word [bp + 4] - disk identifier
BIOS_ResetDiskSubsystem:
        push bp
        mov bp, sp
        push dx    ; save dx in stack
        mov dl, [bp + 4]
    .reset:
        mov ah, 0  ; (int 0x13) Device reset. Causes controller recalibration.
        int 0x13   ; call disk input/output service
        jc .reset  ; if CF=1 an error has happened and we try again.
        
        pop dx     ; restore dx
        mov sp, bp             
        pop bp
        ret 2
; -------------------------------->

; <---- BIOS_GetDiskParameters ----
; Determines parameters of a specified disk.
; Parameters: word [bp + 4] - disk identifier
; Returns: dl - overall number of disks in the system
;          dh - maximum head index
;          ch - maximum cylinder index
;          cl - number of sectors per a track
; Remarks: floppy disk 1.44 MB has 2 heads (0 and 1), 80 tracks (0...79) and 18 sectors per a track (1...18)
BIOS_GetDiskParameters:
    push bp
    mov bp, sp
    push es           ; save es in stack
    
    ; set ES:DI to 0000h:0000h to work around some buggy BIOS
    ;(http://en.wikipedia.org/wiki/INT_13H)
    xor ax, ax        ; ax = 0
    mov es, ax        ; es = 0
    xor di, di        ; di = 0
   
    mov dl, [bp + 4]  ; drive index
    mov ah, 0x08      ; read drive parameters
    int 0x13          ; call disk input/output service

	pop es            ; restore es
    mov sp, bp             
    pop bp
    ret 2
; -------------------------------->

; <---- BIOS_IsExDiskServiceSupported --
; Returns 1 (true) if BIOS supports extended disk service and 0 (false) otherwise.
; Returns: ax=1 or ax=0
BIOS_IsExDiskServiceSupported:
        mov ah, 0x41       ; check extensions present
        mov bx, 0x55AA     ; signature
        int 0x13           ; call disk input/output service
        jc .not_supported  ; if extended disk service is not supported CF flag will be set to 1
        mov ax, 1          ; ax = 1
        ret
    .not_supported:
        xor ax, ax         ; ax = 0
        ret
; -------------------------------->

; <---- LBAtoCHS ------------------
; Function accepts linear sector number (Linear Block Address - LBA) and converts it into a format CYLINDER:HEAD:SECTOR (CHS).
; Parameters: dword [bp + 10] - LBA
;             word  [bp + 6] - Sectors per Track (SPT)
;             word  [bp + 4] - Heads per Cylinder (HPC)
; Returns: ch - CYLINDER
;          dh - HEAD
;          cl - SECTOR
; Remarks: LBA = ((CYLINDER * HPC + HEAD) * SPT) + SECTOR - 1
;          CYLINDER  = LBA  / ( HPC * SPT )
;          temp      = LBA  % ( HPC * SPT )
;          HEAD      = temp / SPT
;          SECTOR    = temp % SPT + 1
LBAtoCHS:
    push bp
    mov bp, sp

    push ax           ; save ax in stack

    ; dx:ax = LBA
    mov dx, [bp + 10]
    mov ax, [bp + 8]

    movzx cx, byte [ebp + 6] ; cx = SPT

    div cx            ; divide dx:ax (LBA) by cx (SPT) (AX = quotient, DX = remainder)
    mov cl, dl        ; CL = SECTOR = remainder
    inc cl            ; sectors are indexed starting from 1

    div byte [bp + 4] ; AL = LBA % HPC; AH = remainder
    mov dh, ah        ; DH = HEAD = remainder
    mov ch, al        ; CH = CYLINDER = quotient

    pop ax            ; restore ax from stack

    mov sp, bp
    pop bp
    ret 8
; -------------------------------->

; <---- BIOS_LoadSectors ----------
; Loads sequential sectors from disk into RAM.
; Parameters: word  [bp + 12] - disk identifier
;             dword [bp + 8]  - Linear Block Address (LBA) of the first sector to load
;             word  [bp + 6]  - number of sectors to load
;             word  [bp + 4]  - memory address (in data segment) at which sectors are to be loaded
; Remarks: this function allows for loading sectors from disk within first 8 GiB
BIOS_LoadSectors:
        push bp
        mov bp, sp

        ; save necessary registers to stack
        push es
        push ax
        push bx
        push dx

        mov ax, ds
        mov es, ax

        mov dl, [bp + 12]      ; DL = disk identifier
        call BIOS_GetDiskParameters

        push dword [bp + 8]    ; push LBA
        push cx                ; push Sectors Per Track
        push dx                ; push Heads Per Cylinder
        call LBAtoCHS          ; ch:dh:cl = Cylinder:Head:Sector

    .read:
        mov dl, [bp + 12]      ; DL = disk identifier
        mov bx, [bp + 4]       ; BX = memory address at which sectors are to be loaded
        mov al, [bp + 6]       ; AL = number of sectors to load
        mov ah, 0x02           ; (int 0x13) Read Sectors From Drive function
        int 0x13               ; call disk input/output service
        jc .read               ; if CF=1, an error occured and we try again

        ; restore registers from stack
        pop dx
        pop bx
        pop ax
        pop es

        mov sp, bp
        pop bp
        ret 10
; -------------------------------->

; <---- BIOS_LoadSectorsEx --------
; Loads sequential sectors from disk into RAM (uses extended disk input/output service).
; Parameters: word  [bp + 12] - disk identifier
;             dword [bp + 8]  - Linear Block Address (LBA) of the first sector to load
;             word  [bp + 6]  - number of sectors to load
;             word  [bp + 4]  - memory address (in data segment) at which sectors are to be loaded
; Remarks: this function assumes that stack segment and data segment are the same (ss == ds)
;          this function allows for loading sectors from disk within first 2 TiB
BIOS_LoadSectorsEx:
        push  bp
        mov   bp, sp
        sub   sp, 16 ; allocate memory (16 bytes) in stack for storing a special
                     ; structure needed for calling bios load-from-disk service
        
        ; save necessary registers to stack
        push  ax
        push  dx
        push  si
        
        mov   byte[bp - 16], 16 ; structure size = 16 bytes
        mov   byte[bp - 15], 0  ; unused, should be zero
        
        mov   ax, [bp + 6]      ; number of sectors to load
        mov   [bp - 14], ax
        
        mov   ax, [bp + 4]      ; memory address at which sectors are to be loaded
        mov   [bp - 12], ax
        
        mov   ax, ds            ; segment to which sectors are to be loaded
        mov   [bp - 10], ax

        mov   ax, [bp + 8]      ; 64-bit sector number (1st word)
        mov   [bp - 8], ax
        
        mov   ax, [bp + 10]     ; 64-bit sector number (2nd word)
        mov   [bp - 6], ax
        
        mov   word[bp - 4], 0   ; 64-bit sector number (3rd word)
        mov   word[bp - 2], 0   ; 64-bit sector number (4th word)

        mov   dl, [bp + 12]     ; DL = disk identifier
        lea   si, [bp - 16]     ; si = structure's address
        mov   ah, 0x42          ; (int 0x13) Extended Read Sectors From Drive function
        int   0x13              ; call disk input/output service

        ; restore registers from stack
        pop   si
        pop   dx
        pop   ax

        mov   sp, bp
        pop   bp
        ret   10
; -------------------------------->

; <---- BIOS_LoadSectorsSmart -----
; Loads sequential sectors from disk into RAM.
; Parameters: word  [bp + 12] - disk identifier
;             dword [bp + 8]  - Linear Block Address (LBA) of the first sector to load
;             word  [bp + 6]  - number of sectors to load
;             word  [bp + 4]  - memory address (in data segment) at which sectors are to be loaded
; Remarks: this function assumes that stack segment and data segment are the same (ss == ds)
BIOS_LoadSectorsSmart:
        call BIOS_IsExDiskServiceSupported
        cmp ax, 0
        je BIOS_LoadSectors
        jmp BIOS_LoadSectorsEx
; -------------------------------->
