; boot_sector.asm
; Real mode bootloader

; ============================================== CODE ==========================================================
use16                 ; generate 16-bit code
org 7C00h             ; the code starts at 0x7C00 memory address

start:
    jmp far dword 0x0000:entr ; makes CS=0, IP=entr
    
entr:
    xor  ax, ax       ; ax = 0
    mov  ds, ax       ; setup data segment ds=ax=0
    cli               ; when we set up stack we need disable interrupts because stack is involved in interrupts handling
    mov  ss, ax       ; setup stack segment ss=ax=0
    mov  sp, 0x7C00   ; stack will grow starting from 0x7C00 memory address
    sti               ; enable interrupts
    
    mov byte [disk_id], dl ; save disk identifier (it appears in dl after the start of PC)

    ; Print message
    push message
    call BIOS_PrintString

    ; Reset disk subsystem
    push [disk_id]
    call BIOS_ResetDiskSubsystem
    
    push [disk_id]        ; push disk identifier
    push dword 1          ; push LBA of the 1st sector to load
    push 1                ; push number of sectors to load
    push second_sector    ; push memory address at which sectors are to be loaded
    call BIOS_LoadSectorsSmart
   
    jmp second_sector

include 'bios_services.inc'

; =============================================== DATA =========================================================
disk_id dw 0x0000 ; disk identifier (0...3 - floppy disks; greater or equal to 0x80 - other disk types)
message db 'Starting bootloader...', 0x0D, 0x0A, 0

finish:
    ; The size of a disk sector is 512 bytes. Boot sector signature occupies the two last bytes.
    ; The gap between the end of the source code and the boot sector signature is filled with zeroes.
    times 510-finish+start db 0 
    db 55h, 0AAh ; boot sector signature

; =============================================== 2nd SECTOR ===================================================
second_sector:
    ; Print message from the 2nd sector
    push message_sector2
    call BIOS_PrintString
    
    cli ; disable maskable interrupts
    hlt ; halt the processor

message_sector2 db 'The second sector has been loaded successfully!', 0x0D, 0x0A, 0
