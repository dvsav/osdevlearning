; descriptor_macros.inc

; Segment descriptor format
; BITS  | SIZE | FIELD
; ------+------+------
; 0-15  |  16  | Limit[0:15]
; 16-39 |  24  | Base[0:23]
; 40-47 |   8  | P DPL[0:1] S Type[0:3]
; 48-55 |   8  | G D/B L AVL Limit[16:19]
; 56-63 |   8  | Base[24:31]

; The following macro defines a code or a data segment descriptor.
; The following assumtions take place:
; P=1 (the segment is present in physical memory)
; S=1 (the segment is not a system segment)
; G=1 (the Limit of the segment is measured in 4-kiB pages)
; D/B=1 L=0 (the segment is a 32-bit segment)
; AVL=0
macro SEGMENT_DESCRIPTOR _Base, _Limit, _DPL, _Type
{
    dw _Limit and 0xFFFF                   ; Limit[0:15]
    dw _Base  and 0xFFFF                   ; Base[0:15]
    db (_Base shr 16) and 0xFF             ; Base[15:23]
    db _Type or (_DPL shr 5) or 10010000b  ; P DPL[0:1] S Type[0:3]
    db 11000000b or (_Limit shr 16)        ; G D/B L AVL Limit[16:19]
    db (_Base shr 24)                      ; Base[24:31]
}

; Segment descriptor types (see Intel 64 and IA-32 Architectures Software Developer’s Manual - 3.4.5.1 Code- and Data-Segment Descriptor Types).
; Can be used as the _Type argument of SEGMENT_DESCRIPTOR macro
DATA_READ_ONLY equ 0000b
DATA_READ_WRITE equ 0010b
DATA_READ_ONLY_EXPAND_DOWN equ 0100b
DATA_READ_WRITE_EXPAND_DOWN equ 0110b
CODE_EXECUTE_ONLY equ 1000b
CODE_EXECUTE_READ equ 1010b
CODE_EXECUTE_ONLY_CONFORMING equ 1110b
CODE_EXECUTE_READ_CONFORMING equ 1111b

; Interrupt gate descriptor format
; BITS  | SIZE | FIELD
; ------+------+------
; 0-15  |  16  | Offset[0:15]
; 16-31 |  16  | Selector[0:15]
; 32-39 |   8  | reserved
; 40-47 |   8  | P DPL[0:1] 0 D 1 1 0
; 48-63 |  16  | Offset[16:31]

; The following macro defines an interrupt gate descriptor.
; The following assumtions take place:
; P=1 (the segment is present in physical memory)
; D=1 (the size of gate is 32 bit)
; DPL=0 (descriptor privilege level = 0)
macro INTERRUPT_GATE_DESCRIPTOR _Selector, _Offset
{
    dw _Offset and 0xFFFF ; Offset[0:15]
    dw _Selector          ; Selector
    db 0                  ; reserved
    db 10001110b          ; P DPL[0:1] 0 D 1 1 0
    dw _Offset shr 16     ; Offset[16:31]
}

; Trap gate descriptor format
; BITS  | SIZE | FIELD
; ------+------+------
; 0-15  |  16  | Offset[0:15]
; 16-31 |  16  | Selector[0:15]
; 32-39 |   8  | reserved
; 40-47 |   8  | P DPL[0:1] 0 D 1 1 1
; 48-63 |  16  | Offset[16:31]

; The following macro defines a trap gate descriptor.
; The following assumtions take place:
; P=1 (the segment is present in physical memory)
; D=1 (the size of gate is 32 bit)
; DPL=0 (descriptor privilege level = 0)
macro TRAP_GATE_DESCRIPTOR _Selector, _Offset
{
    dw _Offset and 0xFFFF ; Offset[0:15]
    dw _Selector          ; Selector
    db 0                  ; reserved
    db 10001111b          ; P DPL[0:1] 0 D 1 1 1
    dw _Offset shr 16     ; Offset[16:31]
}