; protected_mode.inc
    jmp protected_mode_code

; -------------------------------------------- DATA ------------------------------------------------------------
pm_message db 'Hello from protected mode! You can type something. Pressing Esc clears the screen.', 0
gp_message db 'General Protection Exception', 0

; counter being incremented at each interrupt from the system timer
timer_counter dd 0

; cursor position (0...1999). initial value of 160 places the cursor to the 4th row on the screen.
cursor dd 240

; -------------------------------------------- INTERRUPT DESCRIPTOR TABLE --------------------------------------
align 8
idt:
    dq 0 ; 0    #DE   Fault        Error code No     Divide Error
    dq 0 ; 1    #DB   Fault/Trap   Error code No     Debug Exception (For Intel use only)
    dq 0 ; 2     -    Interrupt    Error code No     Nonmaskable external interrupt
    dq 0 ; 3    #BP   Trap         Error code No     Breakpoint
    dq 0 ; 4    #OF   Trap         Error code No     Overflow
    dq 0 ; 5    #BR   Fault        Error code No     BOUND Range Exceeded
    dq 0 ; 6    #UD   Fault        Error code No     Invalid Opcode (Undefined Opcode)
    dq 0 ; 7    #NM   Fault        Error code No     Device Not Available (No Math Coprocessor)
    dq 0 ; 8    #DF   Abort        Error code Zero   Double Fault
    dq 0 ; 9          Fault        Error code Yes    Coprocessor Segment Overrun (reserved)
    dq 0 ; 10   #TS   Fault        Error code Yes    Invalid TSS
    dq 0 ; 11   #NP   Fault        Error code Yes    Segment Not Present
    dq 0 ; 12   #SS   Fault        Error code Yes    Stack-Segment Fault
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, exGP_handler; 13   #GP   Fault   Error code Yes   General Protection
    dq 0 ; 14   #PF   Fault        Error code Yes    Page Fault   
    dq 0 ; 15    -                 Error code No     Intel reserved. Do not use.
    dq 0 ; 16   
    dq 0 ; 17   #MF   Fault        Error code No     x87 FPU Floating-Point Error (Math Fault)
    dq 0 ; 18   #MC   Abort        Error code No     Machine Check
    dq 0 ; 19   #XM   Fault        Error code No     SIMD Floating-Point Exception
    dq 0 ; 20   #VE   Fault        Error code No     Virtualization Exception
    dq 0 ; 21    -                                   Intel reserved. Do not use.
    dq 0 ; 22    -                                   Intel reserved. Do not use.
    dq 0 ; 23    -                                   Intel reserved. Do not use.
    dq 0 ; 24    -                                   Intel reserved. Do not use.
    dq 0 ; 25    -                                   Intel reserved. Do not use.
    dq 0 ; 26    -                                   Intel reserved. Do not use.
    dq 0 ; 27    -                                   Intel reserved. Do not use.
    dq 0 ; 28    -                                   Intel reserved. Do not use.
    dq 0 ; 29    -                                   Intel reserved. Do not use.
    dq 0 ; 30    -                                   Intel reserved. Do not use.
    dq 0 ; 31    -                                   Intel reserved. Do not use.
    ; --- Master PIC ---
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, irq0_handler ; 32    IRQ 0    System timer
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, irq1_handler ; 33    IRQ 1    Keyboard controller
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 34    IRQ 2    Cascaded signals from IRQs 8–15 (from slave PIC)
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 35    IRQ 3    Serial port controller for serial port 2 (shared with serial port 4, if present)
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 36    IRQ 4    Serial port controller for serial port 1 (shared with serial port 3, if present)
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 37    IRQ 5    Parallel port 2 and 3  or  sound card
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 38    IRQ 6    Floppy disk controller
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 39    IRQ 7    Parallel port 1. It is used for printers or for any parallel port if a printer is not present.
    ; --- Slave PIC ----
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 40    IRQ 8    Real-time clock (RTC)
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 41    IRQ 9    Advanced Configuration and Power Interface (ACPI) system control interrupt on Intel chipsets
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 42    IRQ 10   The Interrupt is left open for the use of peripherals
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 43    IRQ 11   The Interrupt is left open for the use of peripherals
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 44    IRQ 12   Mouse on PS/2 connector
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 45    IRQ 13   CPU co-processor  or  integrated floating point unit  or  inter-processor interrupt
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 46    IRQ 14   Primary ATA channel (ATA interface usually serves hard disk drives and CD drives)
    INTERRUPT_GATE_DESCRIPTOR CODE_SELECTOR, int_EOI      ; 47    IRQ 15   Secondary ATA channel

idt_size = $ - idt
   
idtr:
    dw idt_size - 1 ; 16-bit limit of the interrupt descriptor table
    dd idt          ; 32-bit base address of the interrupt descriptor table

; -------------------------------------------- CODE ------------------------------------------------------------
include 'pm_utility_functions.inc'
include 'keyboard.inc'

protected_mode_code:
    push word 1 ; row
    push word 0 ; column
    push pm_message
    call PrintString
    
    ; Disable scan-code translation to set2
    push word KEYBOARD_CONTROLLER_COMMAND_READ_COMMAND_BYTE
    call Keyboard_SendCommand
    call Keyboard_ReadOutputBuffer
    and al, not KEYBOARD_COMMAND_BYTE_XLAT
    push word KEYBOARD_CONTROLLER_COMMAND_WRITE_COMMAND_BYTE
    call Keyboard_SendCommand
    push ax
    call Keyboard_WriteInputBuffer
    
    ; Disable keyboard
    ;push word KEYBOARD_COMMAND_DISABLE
    ;call Keyboard_WriteInputBuffer
    ;call Keyboard_ReadOutputBuffer
    
    ; Enable keyboard
    ;push word KEYBOARD_COMMAND_ENABLE
    ;call Keyboard_WriteInputBuffer
    ;call Keyboard_ReadOutputBuffer

    ; Load IDTR register
    lidt [idtr]

    ; Initialize Programmable Interrupt Controller (PIC)
    iSr_master equ 0x20
    iMr_master equ 0x21
    
    iSr_slave equ 0xA0    
    iMr_slave equ 0xA1
    
    ; <-------- master i8259A PIC initialization --------
    mov al, 00010001b
    out iSr_master, al
    
    ; Define interrupt vector for the 0th line of PIC
    mov al, 0x20 ; (interrupt vector No 32)
    out iMr_master, al
    
    ; Bit mask defines the line of master i8259A to which slave i8259A is connected
    mov al, 00000100b ; (line No 2)
    out iMr_master, al
    
    mov al, 00000001b
    out iMr_master, al
    ; -------------------------------------------------->
    
    ; <-------- slave i8259A PIC initialization ---------
    mov al, 00010001b
    out iSr_slave, al
    
    ; Define interrupt vector for the 0th line of PIC
    mov al, 0x28 ; (interrupt vector No 40)
    out iMr_slave, al
    
    ; Defines the line number through which slave i8259A is connected to master i8259A
    mov al, 00000010b ; (line No 2)
    out iMr_slave, al
    
    mov al, 00000001b
    out iMr_slave, al
    ; -------------------------------------------------->

    ; Enable non-maskable interrupts (NMIs)
    in al, 0x70 ; CMOS register
    and al, 0x7F
    out 0x70, al
    
    ; Enable maskable interrupts
    sti
    
    ; Infinite loop
    jmp $

; -------------------------------------------- INTERRUPT HANDLERS ----------------------------------------------
; ---- End of Interrupt -----------
int_EOI:
    push ax
    ; Reset interrupt controllers
    mov al, 0x20
    out iSr_master, al
    out iSr_slave, al
    pop ax
    iretd

; ---- General Protection Fault ---
exGP_handler:
    pop  eax ; pop error code
    push 0
    push 0
    push gp_message
    call PrintString
    iretd		

; ---- System Timer ---------------
irq0_handler:
    mov byte [es:159], 0x07
    inc byte [timer_counter]    
    cmp byte [timer_counter], 18
    jz @f
    jmp int_EOI
@@:
    mov byte [timer_counter], 0
    cmp byte [es:158], '/'
    jz @f
    mov byte [es:158], '/'
    jmp int_EOI
@@:
    mov byte [es:158], '\'
    jmp int_EOI

KEYBOARD_KEY_IS_SPECIAL_FLAG equ 00000001b
KEYBOARD_KEY_IS_BREAK_FLAG   equ 00000010b
scan_code_flags db 0
; ---- Keyboard Controller --------
irq1_handler:
    push ax
    push edi
    
    in al, 0x60

; <----------------------------------------------
    ;if(al == KEYBOARD_SPECIAL_KEY)
    ;{
    ;    scan_code_flags |= KEYBOARD_KEY_IS_SPECIAL_FLAG;
    ;    return;
    ;}
    ;else if(al == KEYBOARD_BREAK_KEY)
    ;{
    ;    scan_code_flags |= KEYBOARD_KEY_IS_BREAK_FLAG;
    ;    return;
    ;}
    ;else if((scan_code_flags & KEYBOARD_KEY_IS_SPECIAL_FLAG) != 0 || (scan_code_flags & KEYBOARD_KEY_IS_BREAK_FLAG) != 0)
    ;{
    ;    scan_code_flags = 0;
    ;    return;
    ;}
    
    cmp al, KEYBOARD_SPECIAL_KEY
    jnz @f
    or byte [scan_code_flags], KEYBOARD_KEY_IS_SPECIAL_FLAG
    jmp .exit
@@:
    cmp al, KEYBOARD_BREAK_KEY
    jnz @f
    or byte [scan_code_flags], KEYBOARD_KEY_IS_BREAK_FLAG
    jmp .exit
@@:
    test [scan_code_flags], KEYBOARD_KEY_IS_SPECIAL_FLAG
    jz @f
    mov [scan_code_flags], 0
    jmp .exit
@@:
    test [scan_code_flags], KEYBOARD_KEY_IS_BREAK_FLAG
    jz @f
    mov [scan_code_flags], 0
    jmp .exit
; ---------------------------------------------->
    
@@:
    ; convert scan-code to ASCII-code
    dec al
    movzx edi, al
    mov al, [edi+SCAN_CODE_SET2]
    cmp al, 0
    jz .exit
    
    ; print symbol on the screen
    mov ah, 0x07 ; symbol attribute (light gray on black)
    mov edi, [cursor]
    mov [es:edi*2], ax
    
    ; advance cursor position (if(++cursor >= 2000) cursor = 0;)
    inc dword [cursor]
    cmp dword [cursor], 2000
    jb .exit
    mov dword [cursor], 0

.exit:
    pop  edi
    pop  ax
    jmp  int_EOI

; ---- Keyboard Controller --------
irq1_handler_set1:
    push ax
    push edi
    
    in al, 0x60
    
    ; we process key downs only, not key ups
    test al, 0x80
    jnz .exit
    
    dec al
    jnz @f
    
    ; if user presses Esc (scan-code = 1) we clear screen
    call ClearScreen
    mov dword [cursor], 0
    jmp .exit

@@:
    ; convert scan-code to ASCII-code
    and al, 0x7F
    movzx edi, al
    mov al, [edi+SCAN_CODE_SET1]
    cmp al, 0
    jz .exit
    
    ; print symbol on the screen
    mov ah, 0x07 ; symbol attribute (light gray on black)
    mov edi, [cursor]
    mov [es:edi*2], ax
    
    ; advance cursor position (if(++cursor >= 2000) cursor = 0;)
    inc dword [cursor]
    cmp dword [cursor], 2000
    jb .exit
    mov dword [cursor], 0

.exit:
    pop  edi
    pop  ax
    jmp  int_EOI
