; keyboard.inc

; An array of ASCII symbols which reflects keyboard layout. Indexes into this array are keyboard scan-codes.
SCAN_CODE_SET1 db 0,'1234567890-+',0,0,'QWERTYUIOP[]',0,0,'ASDFGHJKL;',"'`",0,0,'ZXCVBNM,./',0,'*',0,' ',0, 0,0,0,0,0,0,0,0,0,0, 0,0,'789-456+1230.',0,0
SCAN_CODE_SET2 db 0,0,0,0,0,0,0,0,0,0,0,0,0,126,0,0,0,0,0,0,'Q','1',0,0,0,'Z','S','A','W','2',0,0,'C','X','D','E','4','3',0,0,' ','V'
               db 'F','T','R','5',0,0,'N','B','H','G','Y','6',0,'J',0,'M',0,'U','7','8',0,0,44,'K','I','O','0','9',0,0,'.','/','L',59,'P','-',0,0,0
               db 39,0,'[','=',0,0,0,0,0,']',0,92,0,0,0,0,0,0,0,0,0,0,0,'1',0,'4','7',0,0,0,'0','.','2','5','6','8',0,0,'0','+','3','-','*','9',0,0,0,0,0,0
               
; AT-PS/2 Keyboard Controller Status Register Bits
KEYBOARD_CONTROLLER_STATUS_PERR equ 10000000b
KEYBOARD_CONTROLLER_STATUS_RxTO equ 01000000b
KEYBOARD_CONTROLLER_STATUS_TO   equ 01000000b
KEYBOARD_CONTROLLER_STATUS_TxTO equ 00100000b
KEYBOARD_CONTROLLER_STATUS_MOBF equ 00100000b
KEYBOARD_CONTROLLER_STATUS_INH  equ 00010000b
KEYBOARD_CONTROLLER_STATUS_A2   equ 00001000b
KEYBOARD_CONTROLLER_STATUS_SYS  equ 00000100b
KEYBOARD_CONTROLLER_STATUS_IBF  equ 00000010b ; if IBF=0, Input Buffer is empty and you can write data to it; if IBF=1 Input Biffer is full and you shouldn't write data to it
KEYBOARD_CONTROLLER_STATUS_OBF  equ 00000001b ; if OBF=1, Output Buffer is full and you can read data from it; if OBF=0 Output Buffer is empty and you shouldn't read data from it

; AT-PS/2 Keyboard Controller's Commands
KEYBOARD_CONTROLLER_COMMAND_READ_COMMAND_BYTE       equ 0x20 ; parameters: no; returns: command byte
KEYBOARD_CONTROLLER_COMMAND_WRITE_COMMAND_BYTE      equ 0x60 ; parameters: command byte
KEYBOARD_CONTROLLER_COMMAND_GET_VERSION_NUMBER      equ 0xA1 ; parameters: no; returns: version number
KEYBOARD_CONTROLLER_COMMAND_GET_PASSWORD_EXISTS     equ 0xA4 ; parameters: no; returns: 0xFA if password exists and 0xF1 otherwise
KEYBOARD_CONTROLLER_COMMAND_SET_PASSWORD            equ 0xA5 ; parameters: null-terminated string of scan codes
KEYBOARD_CONTROLLER_COMMAND_CHECK_PASSWORD          equ 0xA6
PS2_CONTROLLER_COMMAND_DISABLE_MOUSE_INTERFACE      equ 0xA7 ; parameters: no
PS2_CONTROLLER_COMMAND_ENABLE_MOUSE_INTERFACE       equ 0xA8 ; parameters: no
PS2_CONTROLLER_COMMAND_MOUSE_INTERFACE_TEST         equ 0xA9 ; parameters: no; returns: 0x00 - ok, 0x01 - Clock line stuck low, 0x02 - clock line stuck high, 0x03 - data line stuck low, 0x04 - data line stuck high
KEYBOARD_CONTROLLER_COMMAND_CONTROLLER_SELF_TEST    equ 0xAA ; parameters: no; returns: 0x55 if ok
KEYBOARD_CONTROLLER_COMMAND_KEYBOARD_INTERFACE_TEST equ 0xAB ; parameters: no; returns: 0x00 - ok, 0x01 - Clock line stuck low, 0x02 - clock line stuck high, 0x03 - data line stuck low, 0x04 - data line stuck high
KEYBOARD_CONTROLLER_DISABLE_KEYBOARD_INTERFACE      equ 0xAD ; parameters: no
KEYBOARD_CONTROLLER_ENABLE_KEYBOARD_INTERFACE       equ 0xAE ; parameters: no
KEYBOARD_CONTROLLER_COMMAND_GET_VERSION             equ 0xAF
KEYBOARD_CONTROLLER_COMMAND_WRITE_MOUSE_DEVICE      equ 0xD4 ; parameters: value to be sent to PS/2 mouse
KEYBOARD_CONTROLLER_COMMAND_READ_TEST_PORT          equ 0xE0 ; parameters: no; returns: values on test port 

; AT-PS/2 Keyboard Controller's Command Byte flags (see KEYBOARD_CONTROLLER_COMMAND_READ_COMMAND_BYTE and KEYBOARD_CONTROLLER_COMMAND_WRITE_COMMAND_BYTE commands)
KEYBOARD_COMMAND_BYTE_XLAT equ 01000000b ; Translate Scan Codes to set 1 (0 - disabled, 1 - enabled)
KEYBOARD_COMMAND_BYTE_EN2  equ 00100000b ; Disable Mouse (0 - enabled, 1 - disabled)
KEYBOARD_COMMAND_BYTE_EN   equ 00010000b ; Disable keyboard (0 - enabled, 1 - disabled)
KEYBOARD_COMMAND_BYTE_OVR  equ 00001000b ; Inhibit Override (0 - enabled, 1 - disabled)
KEYBOARD_COMMAND_BYTE_SYS  equ 00000100b ; System Flag (0 - power-on value, 1 - BAT code received)
KEYBOARD_COMMAND_BYTE_INT2 equ 00000010b ; Mouse Output Buffer Full Interrupt (0 - disabled, 1 - enabled)
KEYBOARD_COMMAND_BYTE_INT  equ 00000001b ; Output Buffer Full Interrupt (0 - disabled, 1 - enabled)

; AT-PS/2 Keyboard Commands (do not confuse "Keyboard" and "Keyboard Controller")
KEYBOARD_COMMAND_RESET                             equ 0xFF ; Causes keyboard to enter "Reset" mode
KEYBOARD_COMMAND_RESEND                            equ 0xFE ; Used to indicate an error in reception.
KEYBOARD_COMMAND_SET_KEY_TYPE_MAKE                 equ 0xFD ; Allows the host to specify a key that is to send only make codes.
KEYBOARD_COMMAND_SET_KEY_TYPE_MAKE_BREAK           equ 0xFC ; Similar to "Set Key Type Make", but both make codes and break codes are enabled (typematic is disabled).
KEYBOARD_COMMAND_SET_KEY_TYPE_TYPEMATIC            equ 0xFB ; Similar to previous two commands, except make and typematic is enabled; break codes are disabled.
KEYBOARD_COMMAND_SET_ALL_KEYS_TYPEMATIC_MAKE_BREAK equ 0xFA ; Default setting. Make codes, break codes, and typematic repeat enabled for all keys.
KEYBOARD_COMMAND_SET_ALL_KEYS_MAKE                 equ 0xF9 ; Causes only make codes to be sent; break codes and typematic repeat are disabled for all keys.
KEYBOARD_COMMAND_SET_ALL_KEYS_MAKE_BREAK           equ 0xF8 ; Similar to the previous two commands, except only typematic repeat is disabled.
KEYBOARD_COMMAND_SET_ALL_KEYS_TYPEMATIC            equ 0xF7 ; Similar to the previous three commands, except only break codes are disabled. Make codes and typematic repeat are enabled.
KEYBOARD_COMMAND_SET_DEFAULT                       equ 0xF6 ; Load default typematic rate/delay (10.9cps / 500ms), key types (all keys typematic/make/break), and scan code set (2).
KEYBOARD_COMMAND_DISABLE                           equ 0xF5 ; Keyboard stops scanning, loads default values (see "Set Default" command), and waits further instructions.
KEYBOARD_COMMAND_ENABLE                            equ 0xF4 ; Reenables keyboard after disabled using previous command.
KEYBOARD_COMMAND_SET_TYPEMATIC_RATE_DELAY          equ 0xF3 ; Host follows this command with one argument byte that defines the typematic rate and delay.
KEYBOARD_COMMAND_SET_SCAN_CODE_SET                 equ 0xF0 ; Host follows this command with one argument byte, that specifies which scan code set the keyboard should use. 
KEYBOARD_COMMAND_ECHO                              equ 0xEE ; The keyboard responds with "Echo" (0xEE).
KEYBOARD_COMMAND_SET_RESET_LEDS                    equ 0xED ; The host follows this command with one argument byte, that specifies the state of the keyboard's Num Lock, Caps Lock, and Scroll Lock LEDs.

; Flags that can be combined to form an argument of command KEYBOARD_COMMAND_SET_RESET_LEDS
KEYBOARD_LED_CAPS_LOCK   equ 00000100b
KEYBOARD_LED_NUM_LOCK    equ 00000010b
KEYBOARD_LED_SCROLL_LOCK equ 00000001b

; Parts of scan codes
KEYBOARD_SPECIAL_KEY equ 0xE0
KEYBOARD_BREAK_KEY   equ 0xF0

; <---- Keyboard_SendCommand ------
; Sends a command (writes it to port 0x64) to the keyboard controller, located on the motherboard.
; Parameters: word [ebp + 8] - command (can be one of the constants defined above: KEYBOARD_CONTROLLER_COMMAND_* or PS2_CONTROLLER_COMMAND_*)
Keyboard_SendCommand:
    push ebp
    mov ebp, esp
    push ax
    
@@:
    in al, 0x64 ; read Status Byte
    ; test Input Buffer Full (IBF) flag
    test al, KEYBOARD_CONTROLLER_STATUS_IBF
    jnz @b ; wait until IBF=0
    mov al, [ebp + 8] ; al = command
    out 0x64, al ; send command to keyboard controller
    
    pop ax
    mov esp, ebp
    pop ebp
    ret 2
; --------------------------------->

; <---- Keyboard_ReadOutputBuffer ---
; Reads single byte from the keyboard output buffer.
; Returns: al - byte from the keyboard output buffer
Keyboard_ReadOutputBuffer:
.WaitOutputBuffer:
    in al, 0x64 ; read Status Byte
    ; test Output Buffer Full (OBF) flag
    test al, KEYBOARD_CONTROLLER_STATUS_OBF
    jz .WaitOutputBuffer ; wait until OBF=1
    in al, 0x60 ; read output buffer
    ret
; --------------------------------->

; <---- Keyboard_WriteInputBuffer -
; Writes single byte to the keyboard input buffer.
; Parameters: word [ebp + 8] - lower byte of this is to be sent to keyboard
Keyboard_WriteInputBuffer:
.WaitInputBuffer:
    push ebp
    mov ebp, esp
    push ax
    
    in al, 0x64 ; read Status Byte
    ; test Input Buffer Full (IBF) flag
    test al, KEYBOARD_CONTROLLER_STATUS_IBF
    jnz .WaitInputBuffer ; wait until IBF=0
    mov al, [ebp + 8] ; al = parameter
    out 0x60, al ; send byte to keyboard
    
    pop ax
    mov esp, ebp
    pop ebp
    ret 2
; --------------------------------->
