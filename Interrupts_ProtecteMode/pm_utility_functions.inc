; pm_utility_functions.inc

; <---- ClearScreen ---------------
ClearScreen:
    push eax
    push ecx
    push edi

    mov eax, 0x0700 ; symbol (0x00) and its attribute (0x07)
    mov ecx, 2000   ; size of the screen in symbols
    cld             ; direction flag = 0 (means that edi will be incremented)
    xor edi, edi    ; edi = 0
    rep stosw       ; while(ecx != 0) { mov word [es:edi], ax; edi = edi + 2; }

    pop edi
    pop ecx
    pop eax
    ret
; -------------------------------->

; <---- PrintString ---------------
; Prints out a string. The string must be zero-terminated.
; Parameters: dword [ebp + 8] - address of a zero-terminated string
;             word [ebp + 12] - column (0...79)
;             word [ebp + 14] - row (0...24)
; Remarks: This function assumes that the video card is in text mode (CGA-mode)
;          where there are 80 columns * 25 rows each cell comprised of two bytes:
;          1st - the ASCII-code,
;          2nd - the attribute: four most significant bits define background color,
;                               four least significant bits define foreground color.
;          The function also assumes that ES points to a segment descriptor which points to 0xB800 address
;          which is the beginning of video memory in video text mode.
PrintString:
    push ebp
    mov ebp, esp
    
    push eax
    push edi
    push esi
    
    xor eax, eax        ; eax = 0
    mov al, 160         ; al = 80*2
    mov ah, [ebp + 14]  ; ah = row
    mul ah              ; ax = al * ah = 80*2 * row
    add ax, [ebp + 12]  ; ax = ax + column = 80*2 * row + column
    add ax, [ebp + 12]  ; ax = ax + column = 80*2 * row + column*2
    
    mov esi, [ebp + 8]
    mov edi, eax
    cld
    
; Text-printing loop
@@:                          
    lodsb        ; load to al the next charachter located at [ds:esi] address in memory (esi is incremented automatically as DF = 0)
    test al, al  ; test al against zero
    jz @f        ; exit if al is zero
    stosb        ; otherwise move al to memory at address [es:edi] (edi is incremented automatically as DF = 0)
    mov al, 0x07 ; attribute=0x07 (foreground=light grey, background=black)
    stosb        ; move al to memory at address [es:edi] (edi is incremented automatically as DF = 0)
    jmp @b
@@:
    
    pop esi
    pop edi
    pop eax
    
    mov esp, ebp
    pop ebp
    ret 8
; -------------------------------->
