; boot_sector.asm

; ============================================ REAL MODE =======================================================
use16     ; generate 16-bit code
org 7C00h ; the code starts at 0x7C00 memory address

start:
    jmp far dword 0x0000:entr ; makes CS=0, IP=0x7c00

include 'bios_services.inc'

entr:
    xor  ax, ax     ; ax = 0
    mov  ds, ax     ; setup data segment ds=ax=0
    cli             ; when we set up stack we need disable interrupts because stack is involved in interrupts handling
    mov  ss, ax     ; setup stack segment ss=ax=0
    mov  sp, 0x7C00 ; stack will grow starting from 0x7C00 memory address
    sti             ; enable interrupts
    
    call BIOS_ClearScreen
    
    jmp bootloader
    
; -------------------------------------------- BOOTLOADER ------------------------------------------------------
disk_id dw 0x0000 ; disk identifier (0...3 - floppy disks; greater or equal to 0x80 - other disk types)
bootloader_message db 'Starting bootloader...', 0x0D, 0x0A, 0

bootloader:
    mov byte [disk_id], dl ; save disk identifier (it appears in dl after the start of PC)

    ; Print message
    push bootloader_message
    call BIOS_PrintString

    ; Reset disk subsystem
    push [disk_id]
    call BIOS_ResetDiskSubsystem
    
    push [disk_id]            ; push disk identifier
    push dword 1              ; push LBA of the 1st sector to load
    push (finish-start-1)/512 ; push number of sectors to load
    push second_sector        ; push memory address at which sectors are to be loaded
    call BIOS_LoadSectorsSmart
    
    jmp transition_to_pm

; -------------------------------------------- TRANSITION TO PROTECTED MODE ------------------------------------
transition_to_pm:
    ; Open gate A20 through the System Control Port A
    in  al, 0x92 ; read the content of port 0x92 (System Control Port A) into al
    or  al, 0x02 ; set 1st bit in al to 1
    out 0x92, al ; write al into port 0x92
    
    ; Disable maskable interrupts
    cli ; clear flag IF in EFLAGS register
    
    ; Disable non-maskable interrupts
    in  al, 0x70
    or  al, 0x80
    out 0x70, al
   
    lgdt [gdtr] ; load the address and size of Global Descriptor Table (GDT) into GDTR register
    
    ; Switch to protected mode
    mov  eax, cr0  ; read the content of register cr0 (Machine Status Word - MSW) into eax
    or   al,  0x01 ; set 0th bit to 1 (0th bit of cr0 is called PE - Protection Enable)
    mov  cr0, eax  ; write eax to cr0

    ; Load protected mode entry point into CS:EIP
    jmp far dword 0000000000001000b:pm_entry ; 0000000000001000b is a segment selector which is loaded into CS register

; -------------------------------------------- BOOT SECTOR SIGNATURE -------------------------------------------
end_of_boot_sector:
    ; The size of a disk sector is 512 bytes. Boot sector signature occupies the two last bytes.
    ; The gap between the end of the source code and the boot sector signature is filled with zeroes.
    times 510-end_of_boot_sector+start db 0 
    db 55h, 0AAh ; boot sector signature

second_sector:
    
; ============================================ PROTECTED MODE ==================================================
use32 ; generate 32-bit code

; -------------------------------------------- GLOBAL DESCRIPTOR TABLE -----------------------------------------
include 'descriptor_macros.inc'

align 8 ; we align global descriptor table to the 8-bytes boundary (for the sake of processor's performance)
gdt:
    NULL_SEG_DESCRIPTOR db 8 dup(0)
    ;                  Base        Limit    DPL  Type
    SEGMENT_DESCRIPTOR 0x00000000, 0xFFFFF, 00b, CODE_EXECUTE_READ ; code segment descriptor
    SEGMENT_DESCRIPTOR 0x00000000, 0xFFFFF, 00b, DATA_READ_WRITE   ; data and stack segment descriptor
    SEGMENT_DESCRIPTOR 0x000B8000, 0x00008, 00b, DATA_READ_WRITE   ; video memory in text mode segment descriptor

CODE_SELECTOR  equ 0000000000001000b ; code segment selector:  RPL = 0, TI = 0, Descriptor Index = 1
DATA_SELECTOR  equ 0000000000010000b ; data segment selector:  RPL = 0, TI = 0, Descriptor Index = 2
VIDEO_SELECTOR equ 0000000000011000b ; video segment selector: RPL = 0, TI = 0, Descriptor Index = 3
    
gdt_size = $ - gdt

; data to be loaded to GDTR register
gdtr:
    dw gdt_size - 1 ; 16-bit limit of the global descriptor table
    dd gdt          ; 32-bit base address of the global descriptor table
  
; -------------------------------------------- PROTECTED MODE CODE ---------------------------------------------
; Protected mode entry point
pm_entry:
    ; Initialize segment registers (except CS which is already litialized)
    mov ax, DATA_SELECTOR
    mov ds, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    
    ; Load the selector of the video memory segment into ES
    ; because ES is used in string operations like stos, stosw etc.
    mov ax, VIDEO_SELECTOR
    mov es, ax

include 'protected_mode.inc'

finish: